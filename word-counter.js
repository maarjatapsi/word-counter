"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.main = exports.wordCount = exports.checkFilePath = exports.checkCommand = void 0;
// File system module
const fs = require("fs");
// Checks the command length
function checkCommand() {
    const length = process.argv.length;
    if (length !== 3) {
        console.log("Usage: node " + process.argv[1] + " FILENAME");
        return false;
    }
    return true;
}
exports.checkCommand = checkCommand;
// Check if the file exists
function checkFilePath() {
    const filename = process.argv[2];
    if (!fs.existsSync(filename)) {
        console.log("File does not exist: " + filename);
        return false;
    }
    return true;
}
exports.checkFilePath = checkFilePath;
// Count each unique word in the file
function wordCount() {
    const filename = process.argv[2];
    let str = fs.readFileSync(filename, "utf8");
    str = str.toLocaleLowerCase().replace(/\s+/g, " ");
    let words = str
        .replace(/[^\w\s]|_/g, "")
        .split(/\s/)
        .sort();
    const freqMap = {};
    words.forEach(function (word) {
        if (!freqMap[word]) {
            freqMap[word] = 0;
        }
        freqMap[word] += 1;
    });
    return freqMap;
}
exports.wordCount = wordCount;
// Console log word frequency
function main() {
    if (!(checkCommand() && checkFilePath())) {
        process.exit(1);
    }
    else {
        console.log(wordCount());
    }
}
exports.main = main;

"use strict";

// File system module
const fs = require("fs");

// Checks the command length
export function checkCommand(): boolean {
  const length = process.argv.length;
  if (length !== 3) {
    console.log("Usage: node " + process.argv[1] + " FILENAME");
    return false;
  }
  return true;
}

// Check if the file exists
export function checkFilePath(): boolean {
  const filename = process.argv[2];
  if (!fs.existsSync(filename)) {
    console.log("File does not exist: " + filename);
    return false;
  }
  return true;
}

// Count each unique word in the file
export function wordCount() {
  const filename = process.argv[2];
  let str: string = fs.readFileSync(filename, "utf8");
  str = str.toLocaleLowerCase().replace(/\s+/g, " ");
  let words = str
    .replace(/[^\w\s]|_/g, "")
    .split(/\s/)
    .sort();
  const freqMap: any = {};
  words.forEach(function (word) {
    if (!freqMap[word]) {
      freqMap[word] = 0;
    }
    freqMap[word] += 1;
  });
  return freqMap;
}

// Console log word frequency
export function main() {
  if (!(checkCommand() && checkFilePath())) {
    process.exit(1);
  } else {
    console.log(wordCount());
  }
}

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const word_counter_1 = require("../word-counter");
jest.spyOn(console, "log");
describe("Count words", () => {
    process.argv[1] = "../word-counter";
    process.argv[2] = "./__tests__/test.txt";
    it("Check the length of the command line arguments", () => {
        expect((0, word_counter_1.checkCommand)()).toBe(true);
    });
    it("Check if the text file exist", () => {
        expect((0, word_counter_1.checkFilePath)()).toBe(true);
    });
    it("Counts words in the file", () => {
        expect((0, word_counter_1.wordCount)()).toEqual({ hello: 3 });
    });
    describe("Main function works if everything is correct", () => {
        it("Main function works", () => {
            (0, word_counter_1.main)();
            expect(console.log).toHaveBeenCalledWith((0, word_counter_1.wordCount)());
        });
    });
});

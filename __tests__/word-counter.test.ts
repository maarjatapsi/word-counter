import { main, checkFilePath, checkCommand, wordCount } from "../word-counter";

jest.spyOn(console, "log");

describe("Count words", () => {
  process.argv[1] = "../word-counter";
  process.argv[2] = "./__tests__/test.txt";

  it("Check the length of the command line arguments", () => {
    expect(checkCommand()).toBe(true);
  });
  it("Check if the text file exist", () => {
    expect(checkFilePath()).toBe(true);
  });
  it("Counts words in the file", () => {
    expect(wordCount()).toEqual({ hello: 3 });
  });
  describe("Main function works if everything is correct", () => {
    it("Main function works", () => {
      main();
      expect(console.log).toHaveBeenCalledWith(wordCount());
    });
  });
});

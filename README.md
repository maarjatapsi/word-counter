# Word-counter

An app that counts each unique word in a file.

# Getting Started

npm install

# To run test suite

npm run test

# Count words from a file

ts-node index.ts /path/to/file

Theres an example file in root folder that you can use to count.

You can use it as: ts-node index.ts text.txt
